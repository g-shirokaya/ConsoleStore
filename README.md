# ConsoleStore

@pass

admin@mail.ru superadmin oops@gmail.com 12345678

@TASK Implement simple console menu. For example: store | basket | profile | login. 

Navigate throw pages by Users input: "1" leads to "store".

All pages but "login" available only for logged in Users.

Implement login / registration.

Users should be able to execute only Read operations and add(remove) Products to(from) the basket.

Admins should be able to execute all CRUD operations with Users / Products. 
Create additional menu for Admins to save/update Products.

Implement validation for new Users / Products.

Use JDBC and PostgreSQL as a Database.

Create custom exceptions for all cases.

Implement logging of all events.
